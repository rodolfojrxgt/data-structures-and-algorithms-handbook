# Aspiring Nagmamaganda / GP Enjoyer

## Shaina Karillyn G. Pagarigan

👋 Aspiring Nagmamaganda / GP Enjoyer! 🚀👨‍💻 — 💌 shainakarillynpagarigan@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_pagarigan_shainak.jpg](images/bsis_2_pagarigan_shainak.jpg)

### Bio

**Good to know:** I am fond of watching korean dramas and love listening to kpop especially the songs from NCT and EXO. Recently, I also enjoy watching Formula One (F1) and simps for Scuderia Ferrari (CL16 & CS55) because I like the way on how cars can get that fast.

**Motto:** "Always believe that you should never, ever give up and you should always keep fighting even when there's only a slightest chance."

**Languages:** Basic Java, SQL, HTML

**Other Technologies:** MS Office, Canva, Replit, VS Code

**Personality Type:** [Architect (INTJ-T)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

